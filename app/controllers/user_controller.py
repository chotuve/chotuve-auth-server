from app.model.user import User
from app.exceptions.user_exception import UserException


class UserController:

    @staticmethod
    def add(**attributes):
        return User.add(**attributes)

    @staticmethod
    def update(user_id, **attributes):
        return User.update(user_id, **attributes)

    @staticmethod
    def delete(**attributes):
        _id = attributes['id']
        User.delete(_id)

    @staticmethod
    def get_all():
        return User.get_all()

    @staticmethod
    def get_by_id(_id):
        user = User.get_by_id(_id)
        if not user:
            raise UserException(f"User {_id} not found.")
        return user
