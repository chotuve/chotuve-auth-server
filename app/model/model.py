from app import db
from app.exceptions.user_exception import UserException


class Model(db.Model):
    __abstract__ = True

    @staticmethod
    def commit_changes():
        db.session.commit()

    @classmethod
    def get_all(cls):
        data = cls.query.all()
        return data

    @classmethod
    def get_by_id(cls, _id):
        data = cls.query.get(_id)
        return data

    @classmethod
    def add(cls, **kwargs):
        instance = cls(**kwargs)
        db.session.add(instance)
        cls.commit_changes()
        return instance

    @classmethod
    def delete(cls, _id):
        cls.query.get(_id).delete()
        cls.commit_changes()

    @classmethod
    def update(cls, _id, **kwargs):
        instance = cls.query.get(_id)
        if not instance:
            raise UserException(f"User {_id} not found")
        for key in kwargs:
            if key:
                setattr(instance, key, kwargs[key])
        db.session.add(instance)
        cls.commit_changes()
        return instance
