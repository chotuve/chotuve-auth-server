from app.model.model import Model
from app import db


class User(Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    display_name = db.Column(db.String(60))
    email = db.Column(db.String(30), unique=True)
    phone_number = db.Column(db.String(15))
    photo_url = db.Column(db.String(100))

    def __init__(self, display_name, email, phone_number=None, photo_url=None):
        self.display_name = display_name
        self.email = email
        self.phone_number = phone_number
        self.photo_url = photo_url

    def __eq__(self, other):
        if not isinstance(other, User):
            return False
        if self.display_name != other.display_name \
                or self.email != other.email \
                or self.phone_number != other.phone_number \
                or self.photo_url != other.photo_url:
            return False
        return True

    def __str__(self):
        return "{} {} {} {} {}".format(self.id,
                                       self.display_name,
                                       self.email,
                                       self.phone_number,
                                       self.photo_url)

    def serialize(self):
        return {
            'id': self.id,
            'display_name': self.display_name,
            'email': self.email,
            'phone_number': self.phone_number,
            'photo_url': self.photo_url
        }
