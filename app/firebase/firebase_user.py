from firebase_admin import auth


class FirebaseUser:
    @staticmethod
    def create_user(email,
                    password,
                    display_name,
                    photo_url,
                    phone_number=None):
        firebase_user = auth.create_user(
            email=email,
            phone_number=phone_number,
            password=password,
            display_name=display_name,
            photo_url=photo_url)
        print('Sucessfully created new user: {0}'.format(firebase_user.uid))
        return firebase_user

    @staticmethod
    def get_user_by_uid(uid):
        firebase_user = auth.get_user(uid)
        print('Successfully fetched user data: {0}'.format(firebase_user.uid))
        return firebase_user

    @staticmethod
    def get_user_by_email(email):
        firebase_user = auth.get_user_by_email(email)
        print('Successfully fetched user data: {0}'
              .format(firebase_user.email))
        return firebase_user

    # pylint: disable=too-many-arguments
    @staticmethod
    def update_user(uid,
                    email,
                    password,
                    display_name,
                    photo_url,
                    phone_number):
        firebase_user = auth.update_user(
            uid,
            email=email,
            phone_number=phone_number,
            password=password,
            display_name=display_name,
            photo_url=photo_url)
        print('Sucessfully updated user: {0}'.format(firebase_user.uid))
        return firebase_user
