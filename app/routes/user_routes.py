from http import HTTPStatus
from flask import request, jsonify
from flask_restful import Resource
from sqlalchemy import exc
from app.controllers.user_controller import UserController
from app.exceptions.user_exception import UserException


class UserRoute(Resource):
    @staticmethod
    def get(user_id=None):
        if user_id:
            try:
                user = UserController.get_by_id(user_id)
                return jsonify(user.serialize()), HTTPStatus.OK
            except UserException as u_e:
                return u_e.message, HTTPStatus.BAD_REQUEST
        users = UserController.get_all()
        return jsonify([user.serialize() for user in users])

    @staticmethod
    def post():
        content = request.get_json()
        try:
            saved_user = UserController.add(**content)
            response = jsonify(saved_user.serialize())
            response.status_code = HTTPStatus.CREATED
            return response
        except exc.IntegrityError as i_e:
            print(i_e.args)
            return f"Email {content['email']} already exists", \
                   HTTPStatus.BAD_REQUEST

    @staticmethod
    def put(user_id):
        content = request.get_json()
        try:
            updated_user = UserController.update(user_id, **content)
            response = jsonify(updated_user.serialize())
            response.status_code = HTTPStatus.OK
            return response
        except UserException as u_e:
            return u_e.message, HTTPStatus.BAD_REQUEST
