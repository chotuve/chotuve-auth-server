from tests.base_test import BaseTest
from app import db
from app.controllers.user_controller import UserController
from tests.assets.user_builder import UserBuilder


class UserControllerTest(BaseTest):

    def test_get_all_users_returns_collection_with_users(self):
        expected_value_1 = UserBuilder.build_user(1)
        expected_value_2 = UserBuilder.build_user(2)
        db.session.add(expected_value_1)
        db.session.add(expected_value_2)
        db.session.commit()

        actual_value = UserController.get_all()

        self.assertTrue(expected_value_1 in actual_value)
        self.assertTrue(expected_value_2 in actual_value)
