from base_test import BaseTest
from app import db
from app.model.user import User


class UserTest(BaseTest):

    def test_create_an_user_succesfully(self):
        user_data = {
            "display_name": "Doe Jane",
            "email": "doe.jane@mail.com",
            "phone_number": "+54012345678",
            "photo_url": "http://www.example.com/12345678/photo.png"
        }
        user = User.add(**user_data)

        assert user in db.session

    def test_update_an_user_successfully(self):
        user_data = {
            "display_name": "Doe Jane",
            "email": "doe.jane@mail.com",
            "phone_number": "+54012345678",
            "photo_url": "http://www.example.com/12345678/photo.png"
        }
        User.add(**user_data)

        updated_user_data = {
            "display_name": "Smith Jane",
            "email": "smith.jane@mail.com",
            "phone_number": "+54012345678",
            "photo_url": "http://www.example.com/12345678/photo.png"
        }
        user = User.update(1, **updated_user_data)

        assert user in db.session
        self.assertEqual(User(**updated_user_data), user)

    def test_get_by_id(self):
        user_data = {
            "display_name": "Doe Jane",
            "email": "doe.jane@mail.com",
            "phone_number": "+54012345678",
            "photo_url": "http://www.example.com/12345678/photo.png"
        }
        expected_value = User(**user_data)
        db.session.add(expected_value)
        db.session.commit()

        actual_value = User.get_by_id(1)
        self.assertEqual(expected_value, actual_value)

    def test_get_all_users(self):
        user_1_data = {
            "display_name": "Doe Jane",
            "email": "doe.jane@mail.com",
            "phone_number": "+54012345678",
            "photo_url": "http://www.example.com/12345678/photo.png"
        }
        user_2_data = {
            "display_name": "Smith Jon",
            "email": "smith.jon@mail.com",
            "phone_number": "+54345645678",
            "photo_url": "http://www.example.com/64537289/photo.png"
        }
        expected_value_1 = User(**user_1_data)
        expected_value_2 = User(**user_2_data)
        db.session.add(expected_value_1)
        db.session.add(expected_value_2)
        db.session.commit()

        actual_value = User.get_all()

        self.assertTrue(expected_value_1 in actual_value)
        self.assertTrue(expected_value_2 in actual_value)
