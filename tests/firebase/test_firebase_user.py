from app.firebase.firebase_user import FirebaseUser


def test_get_user_by_uid_when_uid_is_correct(mocker):
    uid = "A12345"
    expected_value = {
        "uid": uid,
        "display_name": "Doe Jane",
        "email": "doe.jane@mail.com",
        "phone_number": "+54012345678",
        "photo_url": "http://www.example.com/12345678/photo.png"
    }

    mocker.patch('auth.get_user', return_value=expected_value)

    actual_value = FirebaseUser.get_user_by_uid(uid)
    assert expected_value.uid == actual_value.uid
