from base_test import BaseTest
from app import db
from app.model.user import User


class ModelTest(BaseTest):

    def test_create_an_user_succesfully(self):
        user_data = {
            "display_name": "Doe Jane",
            "email": "doe.jane@mail.com",
            "phone_number": "+54012345678",
            "photo_url": "http://www.example.com/12345678/photo.png"
        }
        user = User.add(**user_data)

        assert user in db.session
