from app.model.user import User


class UserBuilder():

    @staticmethod
    def build_user(id):
        user_data_1 = {
            "display_name": "Doe Jane",
            "email": "doe.jane@mail.com",
            "phone_number": "+54012345678",
            "photo_url": "http://www.example.com/12345678/photo.png"
        }

        user_data_2 = {
            "display_name": "Smith Jon",
            "email": "smith.jon@mail.com",
            "phone_number": "+54345645678",
            "photo_url": "http://www.example.com/64537289/photo.png"
        }

        if id == 1:
            return User(**user_data_1)
        return User(**user_data_2)
