echo "Running pylint"
pylint app/ --disable=missing-docstring --disable=too-many-ancestors --disable=cyclic-import
echo "pylint finished"
