# chotuve-auth-server

Chotuve Auth Server

---
# Migración de la base de datos

Cuando se cambia una tabla se debe ejecutar los siguientes comandos:

* `docker exec -t -i <nombre_container> /bin/bash`
Si no te funciona, ponele "sudo".

Para abrir el container de docker donde se ejecuta la aplicación.

Primer migración `flask db init`

* `flask db migrate -m "Initial migration."`
Efectua la migración.

* `flask db upgrade`
Actualiza la Base de datos con los cambios realizados.

## Para ejecutar un script

Ejecutar el siguiente comando (habiendo levantado el servidor): 
`sudo docker exec <container_name> sh scripts/<scrpit_name>.sh`

Container name:  `chotuve-auth-server_flask_1`


